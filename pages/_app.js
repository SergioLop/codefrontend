import { ApolloProvider } from "@apollo/client";
import client from '../config/apollo';
import PedidoState from '../context/pedidos/PedidoState'
import swal from 'sweetalert';

 

const Myapp = ({ Component, pageProps }) => {
  console.log("Desde _app.js");

   swal("Hello","This webpage (backend)is no longer avaliable by the moment because on changes in the heroku pricing tiers.\nThanks for your understanding. \nFor more details see the console log output. \n message updated by Sergio");
   console.log("=error code=H14 desc='No web processes running' method=OPTIONS path="/" host=frozen-gorge-50536.herokuapp.com request_id=641580c4-7705-4c12-b5d7-9f5dfba051e2 fwd=187.189.168.53 dyno= connect= service= status=503 bytes= protocol=https");
 
  return (
        
    <ApolloProvider client={client}>
        <PedidoState>
           <Component {...pageProps} />
        </PedidoState>
       
    </ApolloProvider>
  );
};
export default Myapp;
