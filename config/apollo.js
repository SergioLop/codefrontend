import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";
import fetch from "node-fetch";
import { setContext } from "apollo-link-context";

//token-header
const httpLink = createHttpLink({
    uri: "https://frozen-gorge-50536.herokuapp.com/",
    fetch,
});

//headers modification
const authLink = setContext((_, { headers }) => {
    //Leer el storage alamacenado

    const token = localStorage.getItem("token");
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : "",
        },
    };
});

const client = new ApolloClient({
    connectToDevTools: true,
    cache: new InMemoryCache(),
    link: authLink.concat(httpLink),
});

export default client;